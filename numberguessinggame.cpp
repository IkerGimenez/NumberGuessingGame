#include "numberguessinggame.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define INPUT_BUFFER_SIZE 4192

int main(void)
{
    bool quitGame = false;
    int64 randMiddle = RAND_MAX / 2;

    srand((uint32)time(0));

    printf("****************\n");
    printf("****************\n\n");
    printf("Hello! I'd like to play a game with you!\n\n");

    do
    {
        int64 numGuesses = 0;

        printf("Please input the smallest number for me to guess (only positive integers): ");

        int64 lowerBound = -1;
        char inputBuffer[INPUT_BUFFER_SIZE] = { 0 };
        char * inputBufferEnd = nullptr;

        do
        {
            gets_s(inputBuffer, INPUT_BUFFER_SIZE);
            lowerBound = strtol(inputBuffer, &inputBufferEnd, 10);

            if(lowerBound < 0)
            {
                printf("Input was invalid. Please make sure you are inputting positive integers only!\n");
            }

        } while(lowerBound < 0);

        printf("\n\n");
        printf("My lower bound for this game is %lld\n\n", lowerBound);

        printf("Please input the largest number for me to guess (only positive integers): ");

        int64 upperBound = -1;

        do
        {
            gets_s(inputBuffer, INPUT_BUFFER_SIZE);
            upperBound = strtol(inputBuffer, &inputBufferEnd, 10);

            if((upperBound < 0) || (lowerBound > upperBound))
            {
                printf("Input was invalid. Please make sure you are inputting positive integers only!\n");
            }

        } while(upperBound < 0);

        printf("\n\n");
        printf("My upper bound for this game is %lld\n\n", upperBound);

        printf("Please input the number I have to guess (make sure it's between %lld and %lld): ", lowerBound, upperBound);

        int64 numberToGuess = -1;

        bool numberToGuessInvalid = (numberToGuess < 0) || (lowerBound > numberToGuess) || (upperBound < numberToGuess);
        do
        {
            gets_s(inputBuffer, INPUT_BUFFER_SIZE);
            numberToGuess = strtol(inputBuffer, &inputBufferEnd, 10);
            
            numberToGuessInvalid = (numberToGuess < 0) || (lowerBound > numberToGuess) || (upperBound < numberToGuess);
            if(numberToGuessInvalid)
            {
                printf("Input was invalid. Please make sure the number is a positive integer and it is larger than %lld and smaller than %lld!\n", lowerBound, upperBound);
            }

        } while(numberToGuessInvalid);

        printf("\n\n");
        printf("The number to guess is %lld\n\n", numberToGuess);

        printf("****************\n");
        printf("****************\n");
        printf("Let's play!\n\n");

        int64 currentGuess = ((upperBound - lowerBound) / 2) + lowerBound;
        bool correctGuess = false;

        do
        {
            int64 userInput = -1;
            printf("I think the number is %lld\n", currentGuess);
            printf("Am I right? (1 = Yes, 2 = Too Low, 3 = Too High): ");
            ++numGuesses;

            do
            {
                gets_s(inputBuffer, INPUT_BUFFER_SIZE);
                userInput = strtol(inputBuffer, &inputBufferEnd, 10);

                printf("\n");

                if((userInput <= 0) || (userInput > 4))
                {
                    printf("Invalid input. Please input 1, 2 or 3 only.\n");
                }

            } while((userInput <= 0) || (userInput > 4));

            // User input YES
            if(userInput == 1)
            {   
                correctGuess = true;

                if(currentGuess != numberToGuess)
                {
                    printf("I know you're lying.\n");
                }
                else
                {
                    printf("Yay! I win!\n");
                    printf("It took me %lld guesses to find the answer.\n\n", numGuesses);
                }

                int64 randDecision = rand();

                if(randDecision > randMiddle)
                {
                    printf("Let's play again!\n\n");
                    continue;
                }
                else
                {
                    printf("I don't want to play anymore.\n\n");
                    quitGame = true;
                }
            }
            else
            {
                if(userInput == 2)
                {
                    lowerBound = currentGuess + 1;
                }
                else if(userInput == 3)
                {
                    upperBound = currentGuess - 1;
                }

                currentGuess = ((upperBound - lowerBound) / 2) + lowerBound;
            }

        } while(correctGuess == false);

    } while(quitGame == false);

    return 0;
}
