import platform
import os
import subprocess
import ycm_core

DIR_OF_THIS_SCRIPT = os.path.abspath(os.path.dirname(__file__))
SOURCE_EXTENSIONS = [ '.cpp' ]

flags = [ '-x', 'c++', '-Wall', '-Wextra', '-Werror', ]

def FlagsForFile( filename, **kwargs ):
        return { 
                'flags': flags,
                'do_cache': True
                }

